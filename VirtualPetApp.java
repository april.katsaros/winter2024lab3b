import java.util.Scanner;

public class VirtualPetApp {

	public static void main(String[] args){
		// creating object, array and scanner
		RedPanda redPanda = new RedPanda();
		RedPanda[] packOfRedPandas = new RedPanda[4];
		Scanner reader = new Scanner(System.in);
		
		// populating array
		for (int i = 0; i < packOfRedPandas.length; i++){
			packOfRedPandas[i] = new RedPanda();
			
			System.out.println("Enter a cute name for the red panda:");
			packOfRedPandas[i].name = reader.nextLine();
			
			System.out.println("Enter a food for the red panda to eat:");
			packOfRedPandas[i].food = reader.nextLine();
			
			System.out.println("Enter the number of hours the red panda will be asleep for:");
			packOfRedPandas[i].hoursAsleep = Integer.parseInt(reader.nextLine());
		}
		
		System.out.println(packOfRedPandas[3].name + ", " + packOfRedPandas[3].food + ", " + packOfRedPandas[3].hoursAsleep);
		
		// calling instance methods
		packOfRedPandas[0].wakeUpPanda();
		packOfRedPandas[0].eatFood();
	}
}