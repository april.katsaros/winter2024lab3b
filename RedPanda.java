public class RedPanda {
	public String name;
	public String food;
	public int hoursAsleep;
	
	public void wakeUpPanda(){
		if (hoursAsleep == 1){
			System.out.println(name + " has only been sleeping for " + hoursAsleep + " hour! Let " + name + " sleep!");
		}
		else if (hoursAsleep >= 0 && hoursAsleep <= 10){
			System.out.println(name + " has only been sleeping for " + hoursAsleep + " hours! Let " + name + " sleep!");
		}
		else {
			System.out.println(name + " has been sleeping for " + hoursAsleep + " hours! Wake up " + name + "!");
		}
	}
	
	public void eatFood(){
		System.out.println("You gave " + name + " some " + food + " to eat! Yum!");
	}
	
}